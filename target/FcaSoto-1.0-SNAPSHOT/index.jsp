<%-- 
    Document   : index
    Created on : 11-04-2020, 17:30:40
    Author     : id20445
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/estilo.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="cuadro">
            <form>
                <p id="titulo">INICIAR SESION</p>
                <hr>
                <br></br>
                <label id="subtitulo1">NOMBRE ESTUDIANTE</label>
                <br></br>
                <input type="text" class="entrada"/>
                <br></br>
                <label id="subtitulo2">INDIQUE N° SECCION</label>
                <br></br>
                <input type="text" class="entrada"/>
                 <br></br>
                <label id="subtitulo3">CONTRASEÑA</label>
                <br></br>
                <input type="password" class="entrada"/>
                <br></br>
                <input type="submit" value="Iniciar Sesion" id="boton"/>
            </form>
            <br/>
            <p id="marca">FcaSoto</p>    
        </div>
             
    </body>
</html>
